<?php

use functions\functions;
require_once dirname(__FILE__) . "/classes/functions.php";

include("header.php");
?>
<html>
	<header>
        <link rel="stylesheet" type="text/css" href="css/pesquisar.css">
    </header>
	<body>
		<h3>Resultados da sua pesquisa</h3><hr><br />
		<?php
			$query = $_GET['query'];

			$min_length = 3;

			if (strlen($query) >= $min_length) {
				$query = htmlspecialchars($query);

                $query = $mysqli->real_escape_string($query);

				$raw_results = $mysqli->query("SELECT * FROM users WHERE (`nome` LIKE '%".$query."%')") or die($mysqli->error);

				if (($raw_results->num_rows) > 0) {
					echo "<br /><br />";
					while ($results = $raw_results->fetch_array()) {
						echo '<a href="profile.php?id='.$results["id"].'" name="p"><br /><p name="p"><h3>'.$results["nome"].' '.$results["apelido"].'</h3></p><br /></a><br /><hr /><br />';
					}
				}else{
				    functions::msgFlash('warning', 'Não foram encontrados resultados para a sua pesquisa!');
				}
			}else{
                functions::msgFlash('error', 'É necessário escrever pelo menos 3 caracteres para realizar uma pesquisa!');
			}
		?>
	</body>
</html>