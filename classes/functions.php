<?php
/**
 * Created by PhpStorm.
 * User: 06012220162
 * Date: 19/10/2018
 * Time: 14:23
 */

namespace functions;

use mysqli;

require_once(realpath(dirname(__FILE__) . '/../classes/functions.php'));

class functions
{
    public static function converteData($data)
    {
        return (preg_match('/\//', $data)) ? implode('-', array_reverse(explode('/', $data))) : implode('/', array_reverse(explode('-', $data)));
    }

    public static function existePedidoAmiszade($login_cookie)
    {
        return
            $mysqli->query(
                "SELECT * 
                  FROM amizades 
                  WHERE para = '$login_cookie' 
                  AND aceite = 'nao' 
                  ORDER BY id DESC;")->fetch_assoc == false ? false : true;
    }

    public static function conexaoDB()
    {
        return  new mysqli("localhost", "root", "", "walpole", 3306);
    }

    public static function msgFlash($tipoMsg, $mensagem)
    {
        require_once(dirname(__FILE__) . '/classes/FlashMessages.php');

        if (!session_id()) @session_start();
        // Instantiate the class
        $msg = new \Plasticbrain\FlashMessages\FlashMessages();

        // Add a few messages
        if ($tipoMsg == 'info') {
            $msg->info($mensagem);
        } elseif ($tipoMsg == 'success'){
            $msg->success($mensagem);
        } elseif ($tipoMsg == 'warning'){
            $msg->warning($mensagem);
        } elseif ($tipoMsg == 'error'){
            $msg->error($mensagem);
        }

        // Display the messages
        return $msg->display();
    }
}