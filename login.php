<?php
    include("db.php");
    if (isset($_POST['entrar'])) {
        $email = $_POST['email'];
        $pass = $_POST['pass'];
        $verifica = $mysqli->query("SELECT * FROM users WHERE email = '$email' AND password='$pass'");
        if (($verifica->num_rows) <= 0) {
            echo "<h3 class='error'>Senha ou e-mail errados!</h3>";
        } else {
            setcookie("login" , $email);
            header("location: ./");
        }
    }

    if (isset($_GET['sair'])) {
        setcookie('login', null, -1);
        header("location: login.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Walpole</title>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
<img src="img/logo.png"><br />
<h2>Login</h2>
<hr>
<h4>Por favor, preencha os seguintes campos para realizar o seu login:</h4>
<form method="POST">
    <input type="email" placeholder="Endereço e-mail" name="email"><br />
    <input type="password" placeholder="Senha" name="pass"><br />
    <input type="submit" value="Entrar" name="entrar">
</form>
<h3>Ainda não tem uma conta? <a href="registar.php">Registrar-me!</a></h3>
</body>
</html>
