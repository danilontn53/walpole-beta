<?php
	include("db.php");

	if (isset($_POST['criar'])) {
		$nome = $_POST['nome'];
		$apelido = $_POST['apelido'];
		$email = $_POST['email'];
		$pass = $_POST['pass'];
		$data = date("Y/m/d");

		$email_check = $mysqli->query("SELECT email FROM users WHERE email='$email'");
		$do_email_check = ($email_check->num_rows);
		if ($do_email_check >= 1) {
			echo '<h3 class=\'error\'>Este endereço de E-mail já está registado, faça o login <a href="login.php">aqui</a></h3>';
		}elseif ($nome == '' OR strlen($nome)<3) {
			echo '<h3 class=\'error\'>Nome inválido!</h3>';
		}elseif ($email == '' OR strlen($email)<10) {
			echo '<h3 class=\'error\'>Endereço de E-mail inválido!</h3>';
		}elseif ($pass == '' OR strlen($pass)<8) {
			echo '<h3 class=\'error\'>Senha inválida, a senha deve ter mais que 8 caracteres!</h3>';
		}else{
			$query = "INSERT INTO users (`nome`,`apelido`,`email`,`password`,`data`) VALUES ('$nome','$apelido','$email','$pass','$data')";
			$data = $mysqli->query($query) or die($mysqli->error);
			if ($data) {
				setcookie("login",$email);
				header("Location: ./");
			}else{
				echo "<h3 class='error'>Desculpa, houve um erro ao realizar o registro, tente novamente mais tarde.</h3>";
			}
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/registrar.css">
</head>
<body>
	<img src="img/logo.png"><br>
	<h2>Criar nova conta</h2>
    <hr>
	<form method="POST">
		<input class="formulario" type="text" placeholder="Primeiro nome" name="nome"><br />
		<input class="formulario" type="text" placeholder="Apelido" name="apelido"><br />
		<input class="formulario" type="email" placeholder="Endereço de E-mail" name="email"><br />
		<input class="formulario" type="password" placeholder="Senha" name="pass"><br />
		<input type="submit" value="Criar conta" name="criar">
	</form>
	<h3>Já tem uma conta? <a href="login.php">Entre aqui!</a></h3>
</body>
</html>