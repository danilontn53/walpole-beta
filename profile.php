<?php
    /**
     * Created by PhpStorm.
     * User: 06012220162
     * Date: 16/10/2018
     * Time: 15:59
     */

use functions\functions;

    require_once dirname(__FILE__) . "/classes/functions.php";
    include("header.php");

    $id = $_GET["id"];
    $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
    $saber = $saberr->fetch_assoc();
    $email = $saber["email"];

    if ($email == $login_cookie) {
        header("Location: myprofile.php");
    }

    $pubs = $mysqli->query("SELECT * FROM pubs WHERE user='$email' ORDER BY id DESC");

    if (isset($_POST['add'])) {

        add();
    }

    function add(){
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        if (!isset($login_cookie)) {
            header("Location: login.php");
        }
        $id = $_GET['id'];
        $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
        $saber = $saberr->fetch_assoc();
        $email = $saber['email'];
        $data = date("Y/m/d");

        $ins = "INSERT INTO amizades (`de`,`para`,`data`) VALUES ('$login_cookie','$email','$data')";
        $conf = $mysqli->query($ins) or die($mysqli->error);
        if ($conf) {
            header("Location: profile.php?id=".$id);
        }else{
            echo "<h3>Erro ao enviar pedido...</h3>";
        }
    }

    if (isset($_POST['cancelar'])) {
        cancel();
    }

    function cancel(){
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        if (!isset($login_cookie)) {
            header("Location: login.php");
        }
        $id = $_GET['id'];
        $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
        $saber = $saberr->fetch_assoc();
        $email = $saber['email'];

        $ins = "DELETE FROM amizades WHERE `de`='$login_cookie' AND para='$email'";
        $conf = $mysqli->query($ins) or die($mysqli->error);
        if ($conf) {
            header("Location: profile.php?id=".$id);
        }else{
            echo "<h3>Erro ao cancelar pedido...</h3>";
        }
    }

    if (isset($_POST['remover'])) {
        remove();
    }

    if (isset($_POST['chat'])) {
        header("Location: chat.php?from=".$id);
    }

    function remove(){
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        if (!isset($login_cookie)) {
            header("Location: login.php");
        }
        $id = $_GET['id'];
        $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
        $saber = $saberr->fetch_assoc();
        $email = $saber['email'];

        $ins = "DELETE FROM amizades WHERE `de`='$login_cookie' AND para='$email' OR `para`='$login_cookie' AND de='$email'";
        $conf = $mysqli->query($ins) or die($mysqli->error);
        if ($conf) {
            header("Location: profile.php?id=".$id);
        }else{
            echo "<h3>Erro ao eliminar amizade...</h3>";
        }
    }

    if (isset($_POST['aceitar'])) {
        aceitar();
    }

    function aceitar(){
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        if (!isset($login_cookie)) {
            header("Location: login.php");
        }
        $id = $_GET['id'];
        $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
        $saber = $saberr->fetch_assoc();
        $email = $saber['email'];

        $ins = "UPDATE amizades SET `aceite`='sim' WHERE `de`='$email' AND para='$login_cookie'";
        $conf = $mysqli->query($ins) or die($mysqli->error);
        if ($conf) {
            header("Location: profile.php?id=".$id);
        }else{
            echo "<h3>Erro ao eliminar amizade...</h3>";
        }
    }

?>
<html>
    <header>
        <link rel="stylesheet" type="text/css" href="css/profile.css">
    </header>
<body>
    <?php
        if ($saber["img"]=="") {
            echo '<img src="img/user.png" id="profile">';
        }else{
            echo '<img src="upload/'.$saber["img"].'" id="profile">';
        }
    ?>
    <div id="menu">
        <form method="POST">
            <h2><?php echo $saber['nome']." ".$saber['apelido']; ?></h2><br />
            <?php
            $amigos = $mysqli->query("SELECT * FROM amizades WHERE de='$login_cookie' AND para='$email' OR para='$login_cookie' AND de='$email'");
            $amigoss = $amigos->fetch_assoc();
            if ($amigos->num_rows >=1 AND $amigoss["aceite"]=="sim") {
                echo '<input type="submit" value="Remover amigo" name="remover"><input type="submit" name="chat" value="Conversar"><input type="submit" name="denunciar" value="Denunciar">';
            }elseif (($amigos->num_rows)>=1 AND $amigoss["aceite"]=="nao" AND $amigoss["para"]==$login_cookie){
                echo '<input type="submit" value="Aceitar pedido" name="aceitar"><input type="submit" name="chat" value="Conversar"><input type="submit" name="denunciar" value="Denunciar">';
            }elseif (($amigos->num_rows)>=1 AND $amigoss["aceite"]=="nao" AND $amigoss["de"]==$login_cookie){
                echo '<input type="submit" value="Cancelar pedido" name="cancelar"><input type="submit" name="chat" value="Conversar"><input type="submit" name="denunciar" value="Denunciar">';
            }else{
                echo '<input type="submit" value="Adicionar amigo" name="add"><input type="submit" name="chat" value="Conversar"><input type="submit" name="denunciar" value="Denunciar">';
            }
            ?>
        </form>
    </div>
    <?php
        $amigoss = $mysqli->query(
                "SELECT * 
                      FROM amizades 
                      WHERE de='$login_cookie' 
                      AND para='$email' 
                      AND aceite = 'sim' 
                      OR para='$login_cookie' 
                      AND de='$email' 
                      AND aceite = 'sim'");

        $amigos = $amigoss->num_rows;
        if ($amigos==1) {
            while ($pub = $pubs->fetch_assoc()) {
                $email = $pub['user'];
                $saberr = $mysqli->query("SELECT * FROM users WHERE email='$email'");
                $saber = $saberr->fetch_assoc();
                $nome = $saber['nome'] . " " . $saber['apelido'];
                $id = $pub['id'];

                if ($pub['imagem'] == "") {
                    echo '<div class="pub" id="' . $id . '">
                            <p><a href="profile.php?id=' . $saber['id'] . '">' . $nome . '</a> - ' . \functions\functions::converteData($pub["data"]) . '</p>
                            <span>' . $pub['texto'] . '</span><br />
                        </div>';
                } else {
                    echo '<div class="pub" id="' . $id . '">
                            <p><a href="profile.php?id=' . $saber['id'] . '">' . $nome . '</a> - ' . \functions\functions::converteData($pub["data"]) . '</p>
                            <span>' . $pub['texto'] . '</span>
                            <img src="upload/' . $pub["imagem"] . '" />
                        </div>';
                }
            }
        } elseif ($amigos==0) {
            $saberr = $mysqli->query("SELECT * FROM users WHERE email='$email'");
            $saber = $saberr->fetch_assoc();
            $nome = $saber['nome'] . " " . $saber['apelido'];

            echo '<div class="pub" id="' . $id . '">
                <p>Aviso</p>
                <span>É necessário ser amigo(a) de '.$nome.' para poder ver as suas publicações.</span><br \>
            </div>';
        }
    ?>
    <br />
    <div id="footer">
        <p class="conteudo">&copy; <?="Walpole, " . date('Y') . " - Todos os direitos reservados"?></p>
    </div>
    <br /><br /><br /><br />
</body>
</html>