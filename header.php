<?php
	include("db.php");
    require_once dirname(__FILE__) . "/classes/functions.php";

    $login_cookie = $_COOKIE['login'];
	if (!isset($login_cookie)) {
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div id="topo">
		<a href="index.php"><img src="img/logo.png" width="100" name="logo" title="Walpole">
        <span class="beta">Beta</span></a>
		<form method="GET" action="pesquisar.php">
		<input type="text" placeholder="Pesquisa alguém..." name="query" autocomplete="off"><input type="submit" hidden>
		</form>

        <a href="login.php?sair='.$login_cookie.'" title="Sair">
            <span class="glyphicon glyphicon-log-out" id="logout"></span>
        </a>
        <a href="notificacoes.php" title="Notificações"><img src="img/notificacoes.png" width="35" name="menu"></a>
        <a href="inbox.php" title="Conversas"><img src="img/chat.png" width="30" name="menu"></a>
        <?php

        $result = $mysqli->query(
            "SELECT * 
                  FROM amizades 
                  WHERE para = '$login_cookie' 
                  AND aceite = 'nao' 
                  ORDER BY id DESC;")->fetch_assoc();

        if ($result != false) {
            echo '<a href="pedidos.php" title="Pedidos de amizade"><img src="img/pedidos.png" name="menu" class="pedidos"></a>';
        }
        ?>
		<a href="myprofile.php" title="Meu perfil"><img src="img/perfil.png" width="30" name="menu"></a>
	</div>
</body>
</html>