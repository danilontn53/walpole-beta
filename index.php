<?php
/**
 * Created by PhpStorm.
 * User: 06012220162
 * Date: 16/10/2018
 * Time: 15:59
 */

use functions\functions;

    define('dirname(__FILE__)', dirname(__FILE__));

    require_once dirname(__FILE__) . "/classes/functions.php";

    include("header.php");

    $pubs = $mysqli->query("SELECT
			T.id, 
		    T.user, 
		    T.texto, 
		    T.imagem, 
		    T.data,
		    U.de,
		    U.para, 
		    U.aceite
		 FROM
		    pubs AS T,
		    amizades AS U 
		 WHERE
		    T.user = U.de AND U.para = '$login_cookie' AND U.aceite='sim'
		    OR T.user = U.para AND U.de = '$login_cookie' AND U.aceite='sim'
		    OR T.user = '$login_cookie'
		    order by T.id DESC;");


    if (isset($_POST['publish'])) {
        if ($_FILES["file"]["error"] > 0) {
            $texto = $_POST["texto"];
            $hoje = date("Y-m-d");

            if ($texto == "") {
                echo "<h3>É necessário que você escreva alguma coisa antes de publicar!</h3>";
            }else{
                $query = "INSERT INTO pubs (`user`,`texto`,`data`) VALUES ('$login_cookie','$texto','$hoje')";
                $data = $mysqli->query($query) or die();
                if ($data) {
                    header("Location: ./");
                }else{
                    echo "Algo de errado aconteceu... Tente outra vez mais tarde";
                }
            }
        }else{
            $n = rand(0, 1000000);
            $img = $n.$_FILES["file"]["name"];

            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/".$img);

            $texto = $_POST['texto'];
            $hoje = date("Y-m-d");

            if ($texto == "") {
                echo "<h3>É necessário que você escreva alguma coisa antes de publicar!</h3>";
            }else{
                $query = "INSERT INTO pubs (`user`,`texto`,`imagem`,`data`) VALUES ('$login_cookie','$texto','$img','$hoje')";
                $data = $mysqli->query($query) or die();
                if ($data) {
                    header("Location: ./");
                }else{
                    echo "Algo de errado aconteceu... Tente outra vez mais tarde";
                }
            }
        }
    }

    if (isset($_GET["love"])) {
        love();
    }

    function love() {
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        $publicacaoid = $_GET['love'];
        $data = date("Y/m/d");

        $post = $mysqli->query("SELECT * FROM pubs WHERE id='$publicacaoid'");
        $postinfo = $post->fetch_assoc();
        $userinfo = $postinfo['user'];

        $ins = "INSERT INTO loves (`user`,`pub`,`date`) VALUES ('$login_cookie','$publicacaoid','$data')";
        $conf = $mysqli->query($ins) or die($mysqli->error);
        if ($conf) {
            $not = $mysqli->query("INSERT INTO notificacoes (`userde`,`userpara`,`tipo`,`post`,`data`) VALUES ('$login_cookie','$userinfo','1','$publicacaoid','$data')");
            header("Location: index.php#".$publicacaoid);
        }else{
            echo "<h3>Erro</h3> ".$mysqli->error;
        }
    }

    if (isset($_GET["unlove"])) {
        unlove();
    }

    function unlove() {
        $mysqli = functions::conexaoDB();

        $login_cookie = $_COOKIE['login'];
        $publicacaoid = $_GET['unlove'];
        $data = date("Y/m/d");

        $del = "DELETE FROM loves WHERE `user`='$login_cookie' AND `pub`='$publicacaoid'";
        $conf = $mysqli->query($del) or die($mysqli->error);
        if ($conf) {
            header("Location: index.php#".$publicacaoid);
        }else{
            echo "<h3>Erro</h3> ".$mysqli->error;
        }
    }
?>
<html>
    <header>
        <link rel="stylesheet" type="text/css" href="css/index.css">
    </header>
<body>
	<div id="publish">
		<form method="POST" enctype="multipart/form-data">
			<br />
			<textarea placeholder="Escreva uma nova publicacão" name="texto"></textarea>
			<label for="file-input">
				<img src="img/imagegrey.png" title="Inserir uma fotografia" />
			</label>
			<input type="submit" value="Publicar" name="publish" />
			<input type="file" id="file-input" name="file" hidden />
		</form>
	</div>
	<?php
		while ($pub=$pubs->fetch_assoc()) {
			$email = $pub['user'];
			$saberr = $mysqli->query("SELECT * FROM users WHERE email='$email'");
			$saber = $saberr->fetch_assoc();
			$nome = $saber['nome']." ".$saber['apelido'];
			$id = $pub['id'];
			$saberLoves = $mysqli->query("SELECT * FROM loves WHERE pub='$id'");
			$loves = $saberLoves->num_rows;

			if ($pub['imagem']=="") {
				echo '<div class="pub" id="'.$id.'">
					<p><a href="profile.php?id='.$saber['id'].'">'.$nome.'</a> - '. functions::converteData($pub["data"]) .'</p>
					<span>'.$pub['texto'].'</span><br />
				</div>
				<div id="love">';
				$email_check = $mysqli->query("SELECT user FROM loves WHERE pub='$id' AND user='$login_cookie'");
				$do_email_check = $email_check->num_rows;
				if ($do_email_check >= 1) {
				    $loves = $loves - 1;
                    $msg =  /*($loves == 1) ? 'Você curtiu' :*/ 'Você e mais '.$loves.' pessoas curtiram isso';
                    echo '<p><a href="index.php?unlove='.$id.'">Curtiu</a> | '.$msg.'</p>';
                } else {
                    echo '<p><a href="index.php?love='.$id.'">Curtir</a> | '.$loves.' pessoas curtiram isso</p>';
                }
                echo '</div>';
			}else{
                echo '<div class="pub" id="'.$id.'">
					<p><a href="profile.php?id='.$saber['id'].'">'.$nome.'</a> - '. functions::converteData($pub["data"]) .'</p>
					<span>'.$pub['texto'].'</span>
                    <img src="upload/'.$pub["imagem"].'" />
				</div>
                <div id="love">';
				$email_check = $mysqli->query("SELECT user FROM loves WHERE pub='$id' AND user='$login_cookie'");
				$do_email_check = $email_check->num_rows;
				if ($do_email_check >= 1) {
				    $loves = $loves - 1;
                    $msg =  /*($loves == 1) ? 'Você curtiu' :*/ 'Você e mais '.$loves.' pessoas curtiram isso';
				    echo '<p><a href="index.php?unlove='.$id.'">Curtiu</a> | '.$msg.'</p>';
                } else {
                    echo '<p><a href="index.php?love='.$id.'">Curtir</a> | '.$loves.' pessoas curtiram isso</p>';
                }
                echo '</div>';
			}
		}
	?>
	<br />
	<div id="footer">
        <p class="conteudo">&copy; <?="Walpole, " . date('Y') . " - Todos os direitos reservados"?></p>
    </div>
    <br /><br /><br /><br />
</body>
</html>