<?php
    require_once dirname(__FILE__) . "/classes/functions.php";

    include("header.php");

	$saberr = $mysqli->query("SELECT * FROM users WHERE email='$login_cookie'");
	$saber = $saberr->fetch_assoc();
	$email = $saber["email"];

	$pubs = $mysqli->query("SELECT * FROM pubs WHERE user='$email' ORDER BY id desc");

	if (isset($_POST['settings'])){
		header("Location: settings.php");
	}

	if (isset($_POST['amigos'])){
		header("Location: amigos.php");
	}
?>
<html>
    <header>
        <link rel="stylesheet" type="text/css" href="css/myprofile.css">
    </header>
<body>
	<?php
		if ($saber["img"]=="") {
			echo '<a href="profilepic.php" title="Alterar foto de perfil" style="width: 100px; display: block; margin: auto;"><img src="img/user.png" id="profile"></a>';
		}else{
			echo '<a href="profilepic.php" title="Alterar foto de perfil" style="width: 100px; display: block; margin: auto;"><img src="upload/'.$saber["img"].'" id="profile"></a>';
		}
	?>
	<div id="menu">
		<form method="POST">
			<h2><?php echo $saber['nome']." ".$saber['apelido']; ?></h2><br />
			<input type="submit" value="Alterar info" name="settings">
            <input type="submit" name="amigos" value="Ver amigos">
		</form>
	</div>
	<?php
		while ($pub=$pubs->fetch_assoc()) {
			$email = $pub['user'];
			$saberr = $mysqli->query("SELECT * FROM users WHERE email='$email'");
			$saber = $saberr->fetch_assoc();
			$nome = $saber['nome']." ".$saber['apelido'];
			$id = $pub['id'];

			if ($pub['imagem']=="") {
				echo '<div class="pub" id="'.$id.'">
					<p><a href="profile.php?id='.$saber['id'].'">'.$nome.'</a> - '. \functions\functions::converteData($pub["data"]) .'</p>
					<span>'.$pub['texto'].'</span><br />
				</div>';
			}else{
				echo '<div class="pub" id="'.$id.'">
					<p><a href="profile.php?id='.$saber['id'].'">'.$nome.'</a> - '. \functions\functions::converteData($pub["data"]) .'</p>
					<span>'.$pub['texto'].'</span>
					<img src="upload/'.$pub["imagem"].'" />
				</div>';
			}
		}
	?>
	<br />
    <div id="footer">
        <p class="conteudo">&copy; <?="Walpole, " . date('Y') . " - Todos os direitos reservados"?></p>
    </div>
    <br /><br /><br /><br />
</body>
</html>