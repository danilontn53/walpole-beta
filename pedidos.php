<?php
/**
 * Created by PhpStorm.
 * User: 06012220162
 * Date: 16/10/2018
 * Time: 15:59
 */

require_once dirname(__FILE__) . "/classes/functions.php";

require_once(dirname(__FILE__) . '/classes/FlashMessages.php');
include("header.php");

if (isset($_GET['succes'])) {
    if (!session_id()) @session_start();
    // Instantiate the class
    $msg = new \Plasticbrain\FlashMessages\FlashMessages();

    // Add a few messages
    '<div class="flahs-succes">';
    $msg->success('Usuário aceito com sucesso!');
    '</div>';
    // Display the messages
    $msg->display();
}

$pubs = $mysqli->query("SELECT * FROM amizades WHERE para = '$login_cookie' AND aceite = 'nao' ORDER BY id DESC;");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
    $saber = $saberr->fetch_assoc();
    $email = $saber['email'];

    $ins = "UPDATE amizades SET `aceite`='sim' WHERE `de`='$email' AND para='$login_cookie'";
    $conf = $mysqli->query($ins) or die($mysqli->error);
    if (!$conf) {
        echo "<h3>Erro ao aceitar amizade...</h3>";
    }else{
        header("Location: pedidos.php?succes=true");
    }
}

if (isset($_GET['remove'])) {
    $id = $_GET['remove'];
    $saberr = $mysqli->query("SELECT * FROM users WHERE id='$id'");
    $saber = $saberr->fetch_assoc();
    $email = $saber['email'];

    $ins = "DELETE FROM amizades WHERE `de`='$login_cookie' AND para='$email' OR `para`='$login_cookie' AND de='$email'";
    $conf = $mysqli->query($ins) or die($mysqli->error);
    if (!$conf) {
        echo "<h3>Erro ao eliminar amizade...</h3>";
    }else{
        header("Location:pedidos.php");
    }
}

?>
<html>
<header>
    <link rel="stylesheet" type="text/css" href="css/pedidos.css">
</header>
<body>
<?php

$result = $mysqli->query(
    "SELECT * 
                  FROM amizades 
                  WHERE para = '$login_cookie' 
                  AND aceite = 'nao' 
                  ORDER BY id DESC;")->fetch_assoc();

if ($result != false) {
    while ($pub=$pubs->fetch_assoc()) {
        $email = $pub['de'];
        $saberr = $mysqli->query("SELECT * FROM users WHERE email='$email'");
        $saber = $saberr->fetch_assoc();
        $nome = $saber['nome'] . " " . $saber['apelido'];
        $id = $pub['id'];

        echo '<div class="pub" id="' . $id . '">
                <span>' . $nome . ' quer ser seu amigo(a):</span><br />
                <p><a href="profile.php?id=' . $saber['id'] . '">Visualizar perfil de ' . $nome . '</a></p><br />
                <a href="pedidos.php?id=' . $saber['id'] . '"><input type="submit" value="Sim, aceito" name="add"></a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="profile.php?remove=' . $saber['id'] . '"><input type="submit" value="Não, obrigado" name="remuve"></a>
                <br /><br />
            </div>';
    }
} else {
    echo "<h3>Não existem mais pedidos de amizades.</h3>";
}
?>
<br />
<br />
<div id="footer">
    <p class="conteudo">&copy; <?="Walpole, " . date('Y') . " - Todos os direitos reservados"?></p>
</div>
<br /><br /><br /><br />
</body>
</html>